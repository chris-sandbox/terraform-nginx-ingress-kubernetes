terraform {
  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.2"
    }
  }
  required_version = ">= 0.12"
}

provider "kustomization" {}

data "kustomization_build" "nginx-ingress" {
  path = "${abspath(path.module)}/manifests"
}

resource "kustomization_resource" "nginx-ingress" {
  for_each = data.kustomization_build.nginx-ingress.ids

  manifest = data.kustomization_build.nginx-ingress.manifests[each.value]
}
